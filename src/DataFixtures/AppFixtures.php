<?php

namespace App\DataFixtures;

use App\Entity\Articles;
use App\Entity\Images;
use Cocur\Slugify\Slugify;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);

        for($i=0; $i<10; $i++){
            $faker = Factory::create();
            $article = new Articles();
            $slugify = new Slugify();
            $title = $faker->sentence;
            $article->setTitre($title)
                ->setIntroduction($faker->text)
                ->setContenu($faker->paragraph(3))
                ->setSlug($slugify->slugify($title))
                ->setImageCouverture($faker->imageUrl());
            $manager->persist($article);

            for($j=0; $j<3; $j++){
                $image = new Images();
                $image->setUrl($faker->imageUrl())->setCaption("titre$j")->setArticles($article);
                $manager->persist($image);
            }
        }

        $manager->flush();
    }
}
