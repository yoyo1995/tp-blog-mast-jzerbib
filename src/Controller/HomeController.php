<?php

namespace App\Controller;

use App\Repository\ArticlesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


class HomeController extends AbstractController{

    /**
    +      * @Route("/", name="home")
    +      */
    public function Home(ArticlesRepository $articlesRepository)
    {

        $article = $articlesRepository->findAll();
        $count_article = count($article);
        $limit = 3;
        $articles = $articlesRepository->findBy([],['id'=>"DESC"], $limit);


        return $this->render("index.html.twig", [
            "count_article" => $count_article,
            "articles" => $articles
        ]);
    }
}